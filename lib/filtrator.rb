module Filtrator
  if defined?(Rails)
    require 'filtrator/railtie'
  else
    raise Exception.new("A train cannot travel without it's rails")
  end

  def self.included base
    base.include ActiveFilter
  end
end
