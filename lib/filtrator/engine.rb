module Filtrator
  module Engine
    FILTRATOR_KEYS = [:where, :pagination, :order]

    def self.parse_configuration configuration, arel_entities
      configuration.select!{|k,v| k.to_sym.in?(FILTRATOR_KEYS)}
      configuration.keys.map do |k|
        klass = "#{self.name}::#{k.to_s.capitalize}".constantize
        klass.new config: configuration[k], arel: arel_entities
      end
    end

    def self.apply_query query, parsed_configuration
      parsed_configuration.each do |c|
        query = c.apply(query)
      end
      query
    end
  end
end
