module Filtrator
  class Railtie < Rails::Engine
    config.autoload_paths += Dir["#{config.root}/lib"]
    config.eager_load_paths += Dir["#{config.root}/lib"]
  end
end
