module Filtrator
  module Engine
    class Pagination
      def initialize args
        @pagination = args[:config]
      end

      def apply query
        if @pagination[:limit]
          query = query.limit(@pagination[:limit])
        end
        if @pagination[:offset]
          query = query.offset(@pagination[:offset])
        end
        query
      end
    end
  end
end
