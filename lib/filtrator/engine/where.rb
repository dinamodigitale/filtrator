module Filtrator
  module Engine
    class Where
      CONFIGURATOR_OPERATIONS = [:eq, :not_eq, :in, :not_in, :gt, :lt, :gteq, :lteq, :bt, :not_bt, :like, :not_like]

      def initialize args
        arel = args[:arel]
        @arel_conditions = nil
        @joins = []

        clauses = args[:config].map do |condition|
          if condition.is_a?(Array) and condition.first.is_a?(String)
            condition = condition.second
          end

          field = if condition[:k].is_a?(Hash)
            if related_arel = arel[:joins][condition[:k].keys.first]
              @joins << condition[:k].keys.first
              related_arel[condition[:k].values.first]
            else
              raise StandardError.new("Unknown arelated :#{condition[:k].keys.first}")
            end
          else
            arel[:main][condition[:k]]
          end

          condition[:c].each do |operation|
            if operation.is_a?(Array) and operation.first.is_a?(String)
              operation = operation.second
            end
            relation, operator = if operation.keys.first.first.in?(['|', '&']) # convert this to use regex
              [operation.keys.first[0,1], operation.keys.first[1..-1]]
            else
              ['&', operation.keys.first]
            end

            negate, operator = if operator.first == "n" # convert this to use regex
              [true, operator[1..-1]]
            else
              [false, operator]
            end

            relation = case relation
            when '|'
              :or
            else
              :and
            end

            operand = operation.values.first

            operand = if operand.is_a?(String) and operand.downcase == 'true'
              true
            elsif operand.is_a?(String) and operand.downcase == 'false'
              false
            else
              operand
            end

            if operator.to_sym.in?(CONFIGURATOR_OPERATIONS)
              if operator.to_sym.in?([:like])
                operator = :matches
                operand = "%#{operand}%"
              end

              single_condition = field.send(operator, operand)
              @arel_conditions = if !@arel_conditions
                single_condition
              else
                @arel_conditions.send(relation, single_condition)
              end
            end
          end
        end
      end

      def apply query
        query.joins(@joins.map(&:to_sym)).where(@arel_conditions)
      end
    end
  end
end
