module Filtrator
  module Engine
    class Order
      def initialize args
        @order = args[:config]
      end

      def apply query
        if @order and @order.is_a?(Array)
          query = query.reorder @order
        end
        query
      end
    end
  end
end
