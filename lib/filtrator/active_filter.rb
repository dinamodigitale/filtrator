module Filtrator
  module ActiveFilter
    def self.included base
      base.extend ClassMethods
    end

    module ClassMethods
      def filtrator config
        collection = self.all

        arel_entities = {
          main: arel_table,
          joins: {}
        }

        collection.klass._reflections.each do |ja, assoc|
          arel_entities[:joins][ja] = assoc.class_name.constantize.arel_table
        end

        config = Engine.parse_configuration config, arel_entities
        Engine.apply_query collection, config
      end
    end
  end
end
