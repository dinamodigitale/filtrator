$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "filtrator/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "filtrator"
  s.version     = Filtrator::VERSION
  s.authors     = ["Manuel Burato", "Dinamo Digitale"]
  s.email       = ["contact@manuelburato.it", "info@dinamodigitale.it"]
  s.homepage    = "https://bitbucket.org/dinamodigitale/filtrator"
  s.summary     = "Make AR filters with ease"
  s.description = "Maaakee AR fiiilteeers wiiith eeeaaaaseee (it's a loooong description, bro!)"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "> 5.0.0"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "byebug"
end
