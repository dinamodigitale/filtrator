class Test < ApplicationRecord
  has_many :students
  include Filtrator

  def self.test
    {
      where: [
        {
          k: :id,
          c: [{'|eq' => 1}]
        },
        {
          k: :field1,
          c: [{'|in' => ["dinamo@", "manuel@"]}]
        },
        {
          k: :field2,
          c: [{'eq' => 'supervisor'}]
        },
        {
          k: :field1,
          c: [{'|like' => 'test'}]
        }
        # {
        #   k: {pod: :id},
        #   c: [{'&eq' => 10}]
        # },
      ],
      pagination: { limit: 10, offset: 20 },
      order: [
        { field3: :asc },
        { field4: :desc },
        # { pod: { name: :asc }}
      ]
    }
  end
end
