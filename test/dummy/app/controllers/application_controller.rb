class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def root
    render plain: Test.filtrator(params).to_sql
  end
end
