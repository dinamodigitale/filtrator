# Filtrator
Filtrator is a Filter 'o Matic gem that cooperate with ActiveRecord for building queries using AREL.

## Usage
Simple usage would be:
```ruby
MyModel.filtrator({
  where: [
    {
      k: :id,
      c: [
        { "eq"=>1 }
      ]
    }, {
      k: :field1,
      c: [
        { "|in" => ["testVal1", "testVal2"] }
      ]
    }, {
      k: :field2,
      c: [
        { "eq"=>"testVal3" }
      ]
    }
  ],
  pag: {
    limit: 10, offset: 20
  },
  order: [
    {field3: :asc}, {field4: :desc}
  ]
})
```
would result in:
```sql
SELECT "mytable".*
FROM "mytable"
WHERE
  (("mytable"."id" = 1 OR "mytable"."field1" IN ('testVal1', 'testVal2'))
  AND "mytable"."field2" = 'testVal3')
ORDER BY
  "mytable"."field3" ASC,
  "mytable"."field4" DESC
LIMIT 10
OFFSET 20
```

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'filtrator'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install filtrator
```

## Angular $http params serializing callback
```javascript

var serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + (obj instanceof Array ? '' : p) + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}

```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
